inlets = 1;
outlets = 0;

var n=0;
/*-------------------------------------------------------------*/
function add_buffer(file_path)
{
  var path_parts = file_path.split("/");


  var folder = path_parts[path_parts.length - 2];
  var file_number = path_parts[path_parts.length - 1].split(".")[0];
  var buffer_name = folder + "_" + file_number;



  var p = this.patcher;
  var b = p.newdefault(315, 15 + (30*n), "buffer~", buffer_name);
  b.read(file_path);
 
  ++n;
}
